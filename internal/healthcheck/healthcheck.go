package healthcheck

import (
	"net"
	"time"
)

type Configuration struct {
	TimoutMs int
	NbCheckBeforeFail int
	PauseMs int
}

func IsUrlResponding(url string, ms int) bool {
	_, err := net.DialTimeout("tcp", url,  time.Duration(ms) * time.Millisecond)
	return err == nil
}



