package configuration

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type ConfigClient struct {
	ServiceName string `yaml:"serviceName"`
	PathPrefix string `yaml:"pathPrefix"`
	Redirect string `yaml:"redirect"`
	ACLs map[string]ACL	`yaml:"acls"`
}

type Methods = []string
type AclName = string
type ACL = map[AclName]Methods

//type Client struct {
//	PathPrefix string `yaml:"pathPrefix"`
//	Redirect string     `yaml:"redirect"`
//	ACLs map[string]ACL `yaml:"acls"`
//}


//type ProxyConfiguration struct {
//	Services map[string]Client `yaml:"services"`
//}

func ReadConf(filename string) (ConfigClient,error) {
	configData, err := ioutil.ReadFile(filename)
	if err != nil {
		return ConfigClient{},err
	}

	config := ConfigClient{}
	err = yaml.Unmarshal(configData, &config)
	if err != nil {
		fmt.Println("Unmarshall failed!")
		return ConfigClient{},err
	}
	return config, nil
}
