package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	. "gateway/internal/configuration"
	"log"
	"net"
)

type configuration struct {
	FilePath string
	ServerUrl string
}

func sendConf(serverUrl string, config *ConfigClient)  {
	conn , err := net.Dial("tcp", serverUrl)
	if err != nil {
		log.Fatal(err)
	}
	enc := gob.NewEncoder(conn)
	confToSend := ConfigClient{
		ServiceName: config.ServiceName,
		PathPrefix:  config.PathPrefix,
		Redirect:    config.Redirect,
		ACLs: config.ACLs,
	}
	fmt.Println("Sending: ", confToSend)
	if err := enc.Encode(confToSend); err != nil {
		log.Fatal("Encode error", err)
	}

	_ = conn.Close()
}

func main() {

	addr := flag.String("server","localhost:3002", "Addr")
	filepath := flag.String("file","acl.yml", "file")

	flag.Parse()
	configuration := configuration{
		ServerUrl: *addr,
		FilePath: *filepath,
	}

	config, _ := ReadConf(configuration.FilePath)

	fmt.Println(config)

	sendConf(configuration.ServerUrl,&config)
}

