package main

import (
	"flag"
	"gateway/internal/healthcheck"
	"gateway/pkg/gateway"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

func main() {
	log.SetLevel(log.TraceLevel)

	timeout:= flag.Int("hc-timeout",500, "-hc-timeout=5 in ms")
	retry:= flag.Int("hc-retry",5, "-hc-retry=5")
	pause:= flag.Int("hc-pause",5000, "-hc-retry=5000 in ms")

	httpp:= flag.Int("tcp-port",3001, "-tcp-port=3001")
	tcp:= flag.Int("http-port",3002, "-http-port=3002")
	flag.Parse()

	hcConf := healthcheck.Configuration{
		TimoutMs: *timeout,
		NbCheckBeforeFail: *retry,
		PauseMs: *pause,
	}

	gwConf := gateway.Configuration{
		TcpPort: *tcp,
		HttpPort: *httpp,
		HealthCheckConf: hcConf,
	}

	if os.Getenv("JWT_SECRET") == "" {
		log.Error("JWT_SECRET must be configured")
	}

	server := gateway.New(&gwConf)
	go server.GetConfFromClient()
	go server.HealthCheck()

	if err := http.ListenAndServe(":"+ strconv.Itoa(gwConf.HttpPort), server); err != nil {
		log.Error("Error:", err)
	}
}
