module gateway

go 1.15

require (
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
