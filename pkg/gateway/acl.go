package gateway

import (
	. "gateway/internal/configuration"
)
func findService(config *Proxy, pathPrefix string) (*Service,bool)  {
	for _, value := range config.Services{

		if value.PathPrefix == "/"+ pathPrefix {
			return value, true
		}
	}
	return nil, false
}

func canAccess(acl *ACL, path string, method string) bool {
	if *acl == nil{
		return false
	}
	for pathDefinition, methods := range *acl{
		if pathMatch(pathDefinition,path){
			if methods == nil {
				return true
			}

			return containsValue(methods, method)
		}
	}
	return false
}
