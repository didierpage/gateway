package gateway

import "net/http"

type user struct {
	Token string
	Roles []string
}

func getRoles(token string, roles []string) bool{

	roles[0] = "public"

	if token != ""{
		role,ok := GetRoleOfToken(token)
		if !ok {
			return false
		}
		roles[1] = role
	}


	return true
}

func getUser(req *http.Request) (user, bool){
	bearerToken := req.Header.Get("Authorization")
	jwtToken := ""
	if bearerToken != "" {
		jwtToken = bearerToken[7:]
	}
	roles := make([]string,2)
	ok := getRoles(jwtToken,roles)

	if !ok {
		return user{}, false
	}

	return user{jwtToken,roles},true
}