package gateway

import (
	"fmt"
	. "gateway/internal/configuration"
	log "github.com/sirupsen/logrus"
	"io"
	"math/rand"
	"net/http"
	"regexp"
	"strings"
)

func pathMatch(definition AclName, path string) bool {
	regex := strings.Replace(definition,"/","\\/",-1)
	regex = strings.Replace(regex,"+","[^\\/]+",-1)
	regex = strings.Replace(regex,"*",".*",-1)

	if regex[len(regex) -1] != '/' {
		regex += "/"
	}

	if path[len(path) -1] != '/' {
		path += "/"
	}

	regex = "^" + regex + "$"

	result, _ := regexp.MatchString(regex, path)

	fmt.Println(regex, path, result)
	return result
}

func redirect(wr http.ResponseWriter, req *http.Request, host string, path string){
	req.URL.Scheme= "http"

	client := &http.Client{}
	//Need this
	req.RequestURI = ""

	req.URL.Host= host
	req.URL.Path = path

	log.Println("Redirected to : "+ host+path)
	resp, err := client.Do(req)
	if err != nil {
		http.Error(wr, "Server Error", http.StatusInternalServerError)
		log.Println("ServeHTTP:", err)
	}
	defer resp.Body.Close()

	wr.WriteHeader(resp.StatusCode)
	io.Copy(wr, resp.Body)
}

func (g *gatewayConfiguration) ServeHTTP(wr http.ResponseWriter, req *http.Request) {

	pathPrefix := strings.Split(req.RequestURI,"/")[1]
	path := req.URL.Path[len(pathPrefix)+1:]

	log.Trace("Got request on ", pathPrefix)

	service, ok := findService(&g.Proxy, pathPrefix)

	if !ok {
		http.Error(wr, "Not found", http.StatusNotFound)
		return
	}
	log.Trace("Client :", service)


	user, ok := getUser(req)



	if !ok{
		http.Error(wr, "Not authorized", http.StatusUnauthorized)
		return
	}
	log.Trace("user :", user)

	canAccessTo := false
	found := false
	for _, role := range user.Roles{
		acl, _ := service.ACLs[role]
		if canAccess(&acl, path,req.Method){
			canAccessTo = true
			found = true
			break
		}
	}

	if !found{
		http.Error(wr, "Not found", http.StatusNotFound)
		return
	}
	log.Println(req.RequestURI, user.Roles,"canAccess :", canAccessTo)
	if !canAccessTo{
		http.Error(wr, "Not authorized", http.StatusUnauthorized)
		return
	}


	redirect(wr,req,getClientRandom(service.Clients).Address,path)
}

func getClientRandom(clients []client) *client {
	nbClients := len(clients)

	i := rand.Intn(nbClients)

	return &clients[i]
}