package gateway

import (
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"os"
)

func GetRoleOfToken(jwtToken string) (string,bool){

	secret := os.Getenv("JWT_SECRET")
	token, _ := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(secret), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		role, ok := claims["role"].(string)

		if !ok{
			return "",false
		}
		return role , true
	}
	return "",false
}