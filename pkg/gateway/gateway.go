package gateway

import (
	"encoding/gob"
	"fmt"
	. "gateway/internal/configuration"
	"gateway/internal/healthcheck"
	log "github.com/sirupsen/logrus"
	"net"
	"net/http"
	"strconv"
	"time"
)

type clientName =  string

type client struct {
	Address              string
	nbPingSkipped        int
	name			 string
}

type Configuration struct {
	TcpPort int
	HttpPort int
	HealthCheckConf healthcheck.Configuration
}


type gatewayConfiguration struct {
	Proxy Proxy
	Configuration      Configuration
}

type ServiceName = string

type Service struct {
	Name string
	Clients []client
	ACLs map[string]ACL
	PathPrefix string
}

type Proxy struct {
	Services map[ServiceName]*Service
}

type Gateway interface {
	http.Handler
	GetConfFromClient()
	HealthCheck()
}

func New(configuration *Configuration) Gateway {
	return &gatewayConfiguration{
		Proxy: Proxy{
			Services: make(map[ServiceName]*Service),
		},
		Configuration: *configuration,
	}
}


func (g *gatewayConfiguration) HCService() {

	for serviceIndex, _ := range g.Proxy.Services{
		for clientIndex, _ := range g.Proxy.Services[serviceIndex].Clients{
			service, _ := g.Proxy.Services[serviceIndex]
			g.HCClient(&g.Proxy.Services[serviceIndex].Clients[clientIndex],service , clientIndex )
		}
	}

}

func (g *gatewayConfiguration) HCClient(client *client,service *Service, index int) {
			isClientUp := healthcheck.IsUrlResponding(client.Address, g.Configuration.HealthCheckConf.TimoutMs)

			if !isClientUp {
				client.nbPingSkipped += 1
				log.Info(client.name, "is down", fmt.Sprintf(" %d/%d", client.nbPingSkipped, g.Configuration.HealthCheckConf.NbCheckBeforeFail))
				if client.nbPingSkipped >= g.Configuration.HealthCheckConf.NbCheckBeforeFail {
					service := g.Proxy.Services[service.Name]
					service.Clients = append(g.Proxy.Services[service.Name].Clients[:index], g.Proxy.Services[service.Name].Clients[index+1:]...)
					log.Info(client.name, "Removed from the known client list")
				}
			}else {
				if client.nbPingSkipped != 0 {
					log.Info(client.name, "is up again")
					client.nbPingSkipped = 0
				}
			}
}


func (g *gatewayConfiguration) HealthCheck(){
	for {
		time.Sleep(time.Duration(g.Configuration.HealthCheckConf.PauseMs) * time.Millisecond)
		g.HCService()
	}
}

func (g *gatewayConfiguration) GetConfFromClient() {
	listener, err := net.Listen("tcp", ":"+ strconv.Itoa(g.Configuration.TcpPort))

	log.Println("Listening on", g.Configuration.TcpPort)

	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}

		go g.handleConnection(&conn)

	}
}

func (g *gatewayConfiguration) removeClient(name *clientName){
	fmt.Println(*name, " removed from list")
}

func (g *gatewayConfiguration) handleConnection(conn *net.Conn){
	dec := gob.NewDecoder(*conn)

	receivedConf := ConfigClient{}
	if err := dec.Decode(&receivedConf); err  != nil{
		log.Println("Error in decode:", err)
	}
	log.Info("New client", receivedConf.ServiceName, "with", receivedConf.ACLs)

	service ,found := g.Proxy.Services[receivedConf.ServiceName]

	if !found {
		service = &Service{
			Name: receivedConf.ServiceName,
			Clients: []client{},
			PathPrefix: receivedConf.PathPrefix,
			ACLs: receivedConf.ACLs,
		}
	}

	newClient := client{
		Address:              receivedConf.Redirect,
		nbPingSkipped:        0,

	}

	service.Clients = append(service.Clients,newClient)

	g.Proxy.Services[receivedConf.ServiceName] = service

	defer func(conn net.Conn) {
		_ = conn.Close()
	}(*conn)

	log.Trace("Current config:", g.Proxy)

	for _, service := range g.Proxy.Services{
		fmt.Println(len(service.Clients))
	}
}



